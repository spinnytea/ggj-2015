'use strict';
var util = require('util');
var _ = require('lodash');
var Base = require('./base');
module.exports = Player;

function Player(options) {
  this.maxHealth = 100;

  Base.call(this, _.merge({
    x: 200,
    y: 800,
    r: -Math.PI/2,
    radius: 80,
    image: 'player',
    player_hit: 'player.player_angry1',
    turn_speed: Math.PI*1.5,
    speed: 400,
    //weapon: '', // set at a later time
    health: this.maxHealth
  }, options));

  this.$behavior.push({ service: 'pop_tart.behaviors.player_keysII' });
}
util.inherits(Player, Base);

Player.prototype.processHit = function(weapon){
  this.health -= weapon.damage;
  console.log('Player hit by ' + weapon.image);
  console.log('Health: ' + this.health + ' ' + this.state);

  if (this.health <= 0){
    this.state = 'dying';
  }
};

Player.prototype.healthPercent = function() {
  return this.health / this.maxHealth * 100;
};