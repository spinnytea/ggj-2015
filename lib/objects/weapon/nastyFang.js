'use strict';
var util = require('util');
var Weapon = require('./weapon');
var Hit = require('../hit');

module.exports = NastyFang;

function NastyFang(owner, f5) {
  Weapon.call(this, owner, {
    type: 'nasty_fang',
    radius: 10,
    myX: 30,
    myY: 10,

    r: 0,
    image: 'weapon.nastyfang',
    attackSFX: 'weapon.explosion',

    recoil: function() {
      this.state = 'dying';

      f5.registry.hits.push(new Hit({x: this.x, y: this.y, image: 'boom', radius: 100}));
    },

    damage: 20

  });

  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.checkPlayerCollision' });
}
util.inherits(NastyFang, Weapon);
