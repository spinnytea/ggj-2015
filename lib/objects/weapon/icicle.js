'use strict';
var util = require('util');
var Weapon = require('./weapon');

module.exports = Icicle;

function Icicle(owner) {
  Weapon.call(this, owner, {
    type: 'icicle',
    radius: 40,
    r: 0,
    image: 'weapon.icicle',
    attackSFX: 'weapon.icicle_attack',

    shouldFire: function(f5) { return f5.input.keys.space; },
    distance: 160,
    speed: 1200
  });

  this.$behavior.push({ service: 'pop_tart.behaviors.jab' });
  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.checkCollision' });
}
util.inherits(Icicle, Weapon);
