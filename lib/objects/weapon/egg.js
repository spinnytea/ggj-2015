'use strict';
var util = require('util');
var Weapon = require('./weapon');

module.exports = Egg;

function Egg(owner) {
  Weapon.call(this, owner, {
    type: 'egg',
    radius: 40,
    r: 0,
    image: 'weapon.egg',

    damage: 1,

    shouldFire: function() { return Math.random() < 0.03; },
    extend: 100,
    speed: Math.PI*1.5
  });

  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.swing' });
  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.checkPlayerCollision' });
}
util.inherits(Egg, Weapon);
