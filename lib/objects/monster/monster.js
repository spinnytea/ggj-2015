'use strict';
var util = require('util');
var _ = require('lodash');
var Base = require('../base');
var Weapon = require('../weapon/weapon');

module.exports = Monster;

function Monster(options) {
  Base.call(this, _.merge({
    // defaults

    //roar: all monsters should have a roar
    x: Math.random()*800 + 100,
    y: Math.random()*400 + 500,
    r: -Math.PI/2,
    radius: 100,
    health: 10,
    state: 'alive',
    weapon: new Weapon(this)
  }, options));

  this.$behavior.push({ service: 'pop_tart.behaviors.avoid' });
  this.$behavior.push({ service: 'pop_tart.behaviors.quickDeath' });
}
util.inherits(Monster, Base);

Monster.prototype.processHit = function(){console.log('Hit monster');};