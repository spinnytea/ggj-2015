'use strict';
var util = require('util');
var Monster = require('./monster');

module.exports = ChickenEgg;

function ChickenEgg() {
  Monster.call(this, {
    radius: 30,
    image: 'weapon.egg',
    roar: 'monster.chickencat',
    croak: 'monster.egg_breaking',

    health: 5,

    weapon: ''
  });

  this.$behavior.push({ service: 'pop_tart.behaviors.attack.hatchEgg' });
}
util.inherits(ChickenEgg, Monster);

ChickenEgg.prototype.processHit = function(weapon){
  this.health -= weapon.damage;
  console.log('ChickenEgg hit by ' + weapon.image);
  console.log('Health: ' + this.health + ' ' + this.state);

  if (this.health <= 0){
    this.state = 'dying';
  }
};

ChickenEgg.prototype.processDeath = function(f5) {
    f5.resource.sound[this.croak].play();
};
