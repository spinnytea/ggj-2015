'use strict';
var util = require('util');
var Monster = require('./monster');
var NastyFang = require('../weapon/nastyFang');


module.exports = Bunny;

function Bunny(f5) {
  Monster.call(this, {
    radius: 40,
    image: 'monster.bunny',
    roar: 'monster.bunnycrow',

    health: 3,

    weapon: new NastyFang(this, f5)
  });

  this.$behavior.push({ service: 'pop_tart.behaviors.attack.corneredRabbit' });
}
util.inherits(Bunny, Monster);

Bunny.prototype.processHit = function(weapon){
  this.health -= weapon.damage;
  console.log('Bunny hit by ' + weapon.image);
  console.log('Health: ' + this.health + ' ' + this.state);

  if (this.health <= 0){
    this.state = 'dying';
  }
};