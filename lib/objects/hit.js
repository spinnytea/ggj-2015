'use strict';
var util = require('util');
var _ = require('lodash');
var Base = require('./base');
module.exports = Hit;

function Hit(options) {
  this.maxHealth = 100;

  Base.call(this, _.merge({
    x: 200,
    y: 800,
    radius: 20,
    image: 'hit',

    life: 500
  }, options));

  this.$behavior.push({ service: 'pop_tart.behaviors.expire' });
}
util.inherits(Hit, Base);
