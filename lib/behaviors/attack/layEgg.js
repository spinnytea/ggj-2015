'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

var ChickenEgg = require('../../objects/monster/chickenEgg');

module.exports = require('angular').module('pop_tart.behaviors.attack.layEgg', [])
  .service('pop_tart.behaviors.attack.layEgg', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5) {

  if (Math.random() < 0.007)  {
    var egg = new ChickenEgg();
    egg.x = obj.x;
    egg.y = obj.y;

    f5.registry.monsters.push(egg);
    console.log('Layed an egg at ' + obj.x + ' ' + obj.y);
  }

  
};
