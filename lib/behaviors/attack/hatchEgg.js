'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

var Chicken = require('../../objects/monster/chicken');

module.exports = require('angular').module('pop_tart.behaviors.attack.hatchEgg', [])
  .service('pop_tart.behaviors.attack.hatchEgg', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5, dt) {

  if (!obj.hatchTime) {obj.hatchTime = 0;}

  obj.hatchTime += dt;

  if (obj.hatchTime > 7000) {
    // Create new Chicken
    var chicken = new Chicken();
    chicken.x = obj.x;
    chicken.y = obj.y;

    //Remove egg and add chicken
    f5.registry.monsters = f5.registry.monsters.filter(function(m){return m !== obj;});
    f5.registry.monsters.push(chicken);
  }
  
};
