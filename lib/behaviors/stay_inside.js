'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.stay_inside', [])
  .service('pop_tart.behaviors.stay_inside', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5) {

  // bound the player
  bound(obj, f5.registry.player);

  // bound the monsters
  f5.registry.monsters.forEach(function(m) {
    bound(obj, m);
  });
};


// the inner stays inside of outer
function bound(outer, inner) {
  if((inner.x-inner.radius) < (outer.x-outer.radius)) {
    inner.x = outer.x-outer.radius + inner.radius;
  } else if((inner.x+inner.radius) > (outer.x+outer.radius)) {
    inner.x = outer.x+outer.radius - inner.radius;
  }

  if((inner.y-inner.radius) < (outer.y-outer.radius)) {
    inner.y = outer.y-outer.radius + inner.radius;
  } else if((inner.y+inner.radius) > (outer.y+outer.radius)) {
    inner.y = outer.y+outer.radius - inner.radius;
  }
}