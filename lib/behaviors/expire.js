'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.expire', [])
  .service('pop_tart.behaviors.expire', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5, dt) {
  obj.life -= dt;
  if(obj.life < 0) {
    var idx = f5.registry.hits.indexOf(obj);
    f5.registry.hits.splice(idx, 1);
  }
};
