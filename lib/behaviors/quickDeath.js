'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.quickDeath', [])
  .service('pop_tart.behaviors.quickDeath', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5) {
  if (obj.state === 'dying'){
    console.log(obj.image + ' is dying');
    // First, kill behaviors

    // Now, just remove the monster
    f5.registry.monsters = f5.registry.monsters.filter(function(m){return m !== obj;});
  }
  
};
