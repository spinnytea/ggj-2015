'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');
var Hit = require('../../objects/hit');

module.exports = require('angular').module('pop_tart.behaviors.weapon.checkCollision', [])
  .service('pop_tart.behaviors.weapon.checkCollision', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5) {

  var monsters = f5.registry.monsters;

  monsters.forEach(function(monster){
    if(obj.hasOwnProperty('attacked') && obj.attacked.indexOf(monster) !== -1) return;

    var distx = obj.x - monster.x;
    var disty = obj.y - monster.y;
    if ((distx*distx + disty*disty) < (monster.radius * monster.radius)){
      monster.processHit(obj);

      if (monster.state === 'dying' && monster.processDeath) {
        monster.processDeath(f5);
      }

      f5.registry.hits.push(new Hit({x: monster.x, y: monster.y}));

      if(obj.hasOwnProperty('attacked'))
        obj.attacked.push(monster);
    }
  });
};
