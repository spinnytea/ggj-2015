'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');
var Hit = require('../../objects/hit');

module.exports = require('angular').module('pop_tart.behaviors.weapon.checkPlayerCollision', [])
  .service('pop_tart.behaviors.weapon.checkPlayerCollision', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5) {

  var player = f5.registry.player;
  if(obj.attacked.indexOf(player) !== -1) return;

  var distx = obj.x - player.x;
  var disty = obj.y - player.y;
  if ((distx*distx + disty*disty) < (player.radius * player.radius)) {
    var rando = Math.floor(Math.random() * 5) + 1;
    console.log('rando ', rando);
    if (!f5.resource.sound[player.player_hit].playing()){
      player.player_hit = 'player.player_angry'+rando;
      f5.resource.sound[player.player_hit].play();
    }
    if (obj.attackSFX && obj.attackSFX === 'weapon.explosion'){
      if (!f5.resource.sound[obj.attackSFX].playing()){
        f5.resource.sound[obj.attackSFX].play();
      }
    }
    player.processHit(obj);
    obj.attacked.push(player);
    f5.registry.hits.push(new Hit({x: player.x, y: player.y}));

    if(obj.recoil)
      obj.recoil();
  }
};
