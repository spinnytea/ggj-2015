'use strict';
var util = require('util');
var angular = require('angular');
var io = require('socket.io');
var Behavior = require('force5/core/behavior');

module.exports = angular.module('pop_tart.behaviors.voting', [])
  .service('pop_tart.behaviors.voting', Voting)
  .name;

Voting.$inject = ['$location'];

function Voting ($location) {
  Behavior.call(this);
  this.connection = 'http://' + $location.host() + ':3001';
}

util.inherits(Voting, Behavior);

Voting.prototype.startVote = function (ctx, config) {
  ctx.votes = {};
  // seed the votes
  config.forEach(function(c) { ctx.votes[c.value] = 1; });

  ctx.open = true;
  ctx.socket.emit('startVote', config);
};

Voting.prototype.closeVote = function (ctx) {
  ctx.open = false;
};

Voting.prototype.init = function (f5) {
  var self = this;
  f5.audience = {
    open: false,
    votes: {},
    socket: io(this.connection),
    startVote: function (config) { return self.startVote(f5.audience, config); },
    closeVote: function () { return self.closeVote(f5.audience); }
  };

  f5.audience.socket.on('vote', function (vote) {
    if(f5.audience.open) {
      if (!f5.audience.votes[vote]) f5.audience.votes[vote] = 0;
      f5.audience.votes[vote]++;
      console.log(f5.audience.votes);
    }
  });
};