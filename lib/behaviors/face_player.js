'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.face_player', [])
  .service('pop_tart.behaviors.face_player', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5, dt) {
  var t = Math.atan2(f5.registry.player.y - obj.y, f5.registry.player.x - obj.x);
  t += Math.PI/2;

  if(obj.r > t)
    obj.r -= obj.turn_speed * dt/1e3;
  if(obj.r < t)
    obj.r += obj.turn_speed * dt/1e3;

};
