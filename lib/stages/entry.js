'use strict';
var util = require('util');
var Stage = require('force5/core/stage');

module.exports = require('angular').module('pop_tart.entry', [])
  .service('pop_tart.entry', PStage).name;

PStage.$inject = ['pop_tart.behaviors.voting'];

function PStage(Voting) {
  Stage.call(this);
  this.voting = Voting;
}
util.inherits(PStage, Stage);

PStage.prototype.init = function(f5) {
  this.voting.init(f5);

  // the vote doesn't work the first time you enter the lobby
  // soo... setup the vote now
  // if the vote gets patched, this will just be reset when we get to the lobby
  f5.audience.startVote([
    { title: 'Pick the Sword', value: 'sword' },
    { title: 'Pick the Pop Tart', value: 'poptart' },
    { title: 'Pick the Icicle', value: 'icicle' }
  ]);

  //f5.audience.startVote([
  //  { title: 'Pick the bunny', value: 'bunny' },
  //  { title: 'Pick the chicken', value: 'chicken' }
  //]);

  f5.resource.loadImage('splash', '/resource/Piet_Mondrian,_1909,_View_from_the_Dunes_with_Beach_and_Piers,_Domburg,_MoMA.jpg');
  f5.resource.loadImage('player', '/resource/player.png');
  f5.resource.loadImage('hit', '/resource/hit.png');
  f5.resource.loadImage('boom', '/resource/boom.png');
  f5.resource.loadImage('weapon.sword', '/resource/weapon/sword.png');
  f5.resource.loadImage('weapon.poptart', '/resource/weapon/poptart.png');
  f5.resource.loadImage('weapon.poptartbox', '/resource/weapon/poptartbox.png');
  f5.resource.loadImage('weapon.icicle', '/resource/weapon/icicle.png');
  f5.resource.loadImage('weapon.fireball', '/resource/monster/dragon_attack.png');
  f5.resource.loadImage('weapon.nastyfang', '/resource/monster/bunny_attack.png');
  f5.resource.loadImage('weapon.egg', '/resource/monster/chicken_attack.png');
  f5.resource.loadImage('monster.bunny', '/resource/monster/bunny.png');
  f5.resource.loadImage('monster.bunny.head', '/resource/monster/bunny_head.png');
  f5.resource.loadImage('monster.chicken', '/resource/monster/chicken.png');
  f5.resource.loadImage('monster.chicken.head', '/resource/monster/chicken_head.png');
  f5.resource.loadImage('monster.dragon', '/resource/monster/dragon.png');
  f5.resource.loadImage('monster.dragon.head', '/resource/monster/dragon_head.png');
  f5.resource.loadImage('scene.lobby', '/resource/scene/lobby.png');
  f5.resource.loadImage('scene.grass', '/resource/scene/grass.png');
  f5.resource.loadImage('scene.fire', '/resource/scene/fire.png');
  f5.resource.loadImage('scene.ice', '/resource/scene/ice.png');

  f5.resource.loadSound('player.player_angry1', '/resource/player_angry1.wav');
  f5.resource.loadSound('player.player_angry2', '/resource/player_angry2.wav');
  f5.resource.loadSound('player.player_angry3', '/resource/player_angry3.wav');
  f5.resource.loadSound('player.player_angry4', '/resource/player_angry4.wav');
  f5.resource.loadSound('player.player_angry5', '/resource/player_angry5.wav');
  f5.resource.loadSound('monster.chickencat', ['/resource/monster/chicken_cat.wav']);
  f5.resource.loadSound('monster.bunnycrow', ['/resource/monster/bunny_crow.wav']);
  f5.resource.loadSound('monster.dragonlonghern', ['/resource/monster/dragon_longhern.wav']);
  f5.resource.loadSound('monster.egg_breaking', ['/resource/monster/egg_breaking.wav']);
  f5.resource.loadMusic('scene.background_drums', ['/resource/scene/background_drums.wav']);
  f5.resource.loadMusic('scene.7thSaga_short', ['/resource/scene/7thSaga_short.wav']);
  f5.resource.loadSound('scene.curtains_harp', ['/resource/scene/curtains_harp.wav']);
  f5.resource.loadSound('scene.success_good', ['/resource/scene/success_good.wav']);
  f5.resource.loadSound('scene.success_bad', ['/resource/scene/success_bad.wav']);
  f5.resource.loadSound('scene.splash_sweep', ['/resource/scene/splash_sweep.wav']);
  f5.resource.loadSound('weapon.icicle_attack', ['/resource/weapon/icicle_attack.wav']);
  f5.resource.loadSound('weapon.poptart_attack', ['/resource/weapon/poptart_attack.wav']);
  f5.resource.loadSound('weapon.sword_attack', ['/resource/weapon/sword_attack.wav']);
  f5.resource.loadSound('weapon.explosion', ['/resource/weapon/explosion.wav']);

  f5.registry.splash = {
    x: 500,
    y: 500,
    r: 0,
    radius: 500,
    image: 'splash'
  };

  f5.registry.hits = [];

  this.templateUrl = '/template/splash.html';

  f5.resource.sound['scene.splash_sweep'].play();
};

PStage.prototype.update = function(f5) {
  if(f5.input.keys.space) {
    delete f5.registry.splash;
    f5.changeStage('pop_tart.stage.lobby');
  }
};
